from itertools import chain
from typing import Callable, Iterable, Iterator, Literal, Union

from fontTools.cu2qu import cu2qu
from fontTools.pens.basePen import (
    AbstractPen,
    decomposeQuadraticSegment,  # type: ignore
    decomposeSuperBezierSegment,  # type: ignore
)
from fontTools.svgLib.path import parse_path  # type: ignore
from typing_extensions import Self


BezierPathCommand = Union[
    tuple[Literal["M"], complex],
    tuple[Literal["L"], complex],
    tuple[Literal["Q"], complex, complex],
    tuple[Literal["C"], complex, complex, complex],
    tuple[Literal["Z"]],
]


class BezierPath(tuple[BezierPathCommand]):
    def __str__(self) -> str:
        return path_to_svg(self)

    def subpaths(self) -> Iterator[Self]:
        return map(type(self), split_bezier_path(self))

    def map_nodes(self, f: Callable[[complex], complex]):
        return type(self)(
            (cmd, *map(f, points)) for cmd, *points in self  # type: ignore
        )

    def nodes(self) -> Iterator[complex]:
        return chain.from_iterable(points for _cmd, *points in self)

    @classmethod
    def Parse(cls, d: str):
        return cls(_PathPen.Parse_path(d))


def split_bezier_path(
    path: Iterable[BezierPathCommand],
) -> Iterator[tuple[BezierPathCommand, ...]]:
    subpath: list[BezierPathCommand] = []
    for cmd in path:
        if subpath and cmd[0] == "M":
            yield tuple(subpath)
            subpath.clear()
        subpath.append(cmd)
    if subpath:
        yield tuple(subpath)


def cubic_path_to_quadratic(
    path: Iterable[BezierPathCommand], *, tolerance: float
) -> Iterator[BezierPathCommand]:
    prev = 0j
    first = 0j
    for cmd in path:
        if cmd[0] == "C":
            cu = [
                (prev.real, prev.imag),
                (cmd[1].real, cmd[1].imag),
                (cmd[2].real, cmd[2].imag),
                (cmd[3].real, cmd[3].imag),
            ]
            qu = cu2qu.curve_to_quadratic(cu, tolerance)  # type:ignore
            for p1, p2 in decomposeQuadraticSegment(qu[1:]):  # type: ignore
                yield "Q", complex(*p1), complex(*p2)  # type: ignore
        else:
            yield cmd

        if cmd[0] == "M":
            first = cmd[-1]
        prev = first if cmd[0] == "Z" else cmd[-1]


def path_to_svg(path: Iterable[BezierPathCommand]) -> str:
    def parts():
        for cmd, *points in path:
            args = ",".join(
                map(str, chain.from_iterable((p.real, p.imag) for p in points))
            )
            yield f"{cmd}{args}"

    return " ".join(parts())


class _PathPen(AbstractPen):
    """`fontTools` pen to parse SVG path `d`` string to `BezierPath`"""

    def __init__(self) -> None:
        super().__init__()
        self.path: list[BezierPathCommand] = []

    def moveTo(self, pt: tuple[float, float]) -> None:
        self.path.append(("M", complex(*pt)))

    def lineTo(self, pt: tuple[float, float]) -> None:
        self.path.append(("L", complex(*pt)))

    def qCurveTo(self, *points: tuple[float, float]) -> None:
        for p1, p2 in decomposeQuadraticSegment(points):  # type: ignore
            self.path.append(("Q", complex(*p1), complex(*p2)))  # type: ignore

    def curveTo(self, *points: tuple[float, float]) -> None:
        for p1, p2, p3 in decomposeSuperBezierSegment(points):  # type: ignore
            self.path.append(
                ("C", complex(*p1), complex(*p2), complex(*p3)),  # type: ignore
            )

    def closePath(self) -> None:
        self.path.append(("Z",))

    @classmethod
    def Parse_path(cls, d: str):
        pen = _PathPen()
        parse_path(d, pen)
        return pen.path
