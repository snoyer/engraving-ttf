import re
import time
from dataclasses import replace
from itertools import chain, count
from math import floor, log2
from pathlib import Path
from typing import Any, Iterable, Optional

import fontTools
from fontTools.fontBuilder import FontBuilder
from fontTools.pens.ttGlyphPen import TTGlyphPen
from fontTools.svgLib.path import parse_path


from .bezierpath import BezierPath
from .svgfonts import SvgFont, SvgGlyph

BezierSvgGlyph = SvgGlyph[BezierPath]


class BezierSvgFont(SvgFont[BezierPath]):
    @classmethod
    def Parse_path(cls, d: str) -> BezierPath:
        return BezierPath.Parse(d)


def scale_for_ttf(svgfont: BezierSvgFont) -> BezierSvgFont:
    scale = scaling_factor_for_ttf(svgfont)

    def scale_glyph(glyph: BezierSvgGlyph):
        return replace(
            glyph,
            path=glyph.path.map_nodes(lambda p: cround(p * scale)),
            horiz_adv_x=round(glyph.horiz_adv_x * scale),
        )

    return svgfont.transformed(
        width=lambda v: round(v * scale),
        heights=lambda v: round(v * scale),
        glyphs=scale_glyph,
    )


def svgfont_to_ttf(svgfont: BezierSvgFont, family: str, style: str, ttf_path: Path):
    fontface = svgfont.fontface

    ascent = abs(int(round(fontface.ascent)))
    descent = abs(int(round(fontface.descent)))
    weight = weight_from_style(style)
    linegap = 0
    copyright = svgfont.metadata

    uid_prefix = f"{fontTools.__name__} {fontTools.version}"
    uid_suffix = f"{int(time.time()):0x}"

    builder = FontBuilder(int(round(fontface.units_per_em)), isTTF=True)

    name_strings = dict(
        familyName=dict(en=family),
        styleName=dict(en=style),
        uniqueFontIdentifier=f"{uid_prefix} : {family} {style} : {uid_suffix}",
        fullName=f"{family} {style}",
        psName=re.sub(r"\s", "", f"{family}-{style}"),
        version="Version 001.000",
        copyright=copyright,
        typographicFamily=family,
        typographicSubfamily=style,
    )
    builder.setupNameTable(name_strings)

    def glyph_advance(g: SvgGlyph[Any]):
        return int(round(g.horiz_adv_x or svgfont.horiz_adv_x))

    def draw_glyph(glyph_path: BezierPath):
        pen = TTGlyphPen(None)
        parse_path(str(glyph_path), pen)
        return pen.glyph()

    glyph_list = {
        ".notdef": BezierSvgGlyph("", ".notdef", svgfont.horiz_adv_x, BezierPath()),
        ".null": BezierSvgGlyph("", ".null", 0, BezierPath()),
        **{g.name: g for g in uniquely_named_glyphs(svgfont.glyphs)},
    }.values()

    builder.setupGlyphOrder([g.name for g in glyph_list])
    builder.setupCharacterMap({ord(g.unicode): g.name for g in glyph_list if g.unicode})
    builder.setupGlyf({g.name: draw_glyph(g.path) for g in glyph_list})

    glyf = builder.font["glyf"]
    builder.setupHorizontalMetrics(
        {g.name: (glyph_advance(g), glyf[g.name].xMin) for g in glyph_list}
    )

    builder.setupHorizontalHeader(ascent=ascent, descent=-descent)

    def height_ratio(t: float):
        return int(round((ascent + descent) * t))

    builder.setupOS2(
        sTypoAscender=+ascent,
        sTypoDescender=-descent,
        sTypoLineGap=linegap,
        usWinAscent=+ascent,
        usWinDescent=+descent,
        sxHeight=int(round(fontface.x_height)),
        sCapHeight=int(round(fontface.cap_height)),
        usWeightClass=weight,
        # subscript and superscript offset based on FontForge default output
        ySubscriptXSize=height_ratio(0.65),
        ySubscriptYSize=height_ratio(0.70),
        ySubscriptXOffset=0,
        ySubscriptYOffset=height_ratio(0.14),
        ySuperscriptXSize=height_ratio(0.65),
        ySuperscriptYSize=height_ratio(0.70),
        ySuperscriptXOffset=0,
        ySuperscriptYOffset=height_ratio(0.48),
        yStrikeoutSize=height_ratio(0.05),
        yStrikeoutPosition=height_ratio(0.26),
    )
    builder.setupPost()

    panose = builder.font["OS/2"].panose
    panose.bWeight = min(max(1 + weight // 100, 0x02), 0x0B)
    panose.bFamilyType = 0x02
    panose.bProportion = 0x03

    maxp_table = builder.font["maxp"]
    maxp_table.maxStackElements = 64
    maxp_table.maxSizeOfInstructions = 64

    builder.save(str(ttf_path.expanduser()))


def uniquely_named_glyphs(glyphs: Iterable[SvgGlyph[Any]]):
    seen_names: set[str] = set()
    for glyph in glyphs:
        if glyph.name not in seen_names:
            yield glyph
            seen_names.add(glyph.name)
        else:
            for name in (f"{glyph.name}{i}" for i in count(2)):
                if name not in seen_names:
                    yield replace(glyph, name=name)
                    seen_names.add(name)
                    break


STYLES = set(["normal", "roman", "oblique", "italic"])

WEIGHTS = {
    # from fontforge UI
    "thin": 100,
    "extra-light": 200,
    "light": 300,
    "regular": 400,
    "medium": 500,
    "semi-bold": 600,
    "bold": 700,
    "extra-bold": 800,
    "black": 900,
    # from pango
    "ultra-light": 200,
    "demi-light": 350,
    "book": 380,
    "normal": 400,
    "demi-bold": 600,
    "ultra-bold": 800,
    "heavy": 900,
    "ultra-heavy": 1000,
}


def weight_from_style(style: str, default: int = 400):
    for s in re.split(r"\s+", style):
        if w := WEIGHTS.get(s.lower()):
            return w
    return default


def scaling_factor_for_ttf(font: BezierSvgFont):
    """find the largest scaling factor so that `units_per_em` is a power of 2
    and all glyphs coordinates stay within bounds."""
    face = font.fontface

    p0, p1 = bounds(chain.from_iterable(glyph.path.nodes() for glyph in font.glyphs))
    values = [p0.real, p0.imag, p1.real, p1.imag, face.ascent, face.descent]
    max_abs = max(map(abs, values))

    max_factor = 16383 / max_abs
    power_of_two = 2 ** floor(log2(face.units_per_em * max_factor))

    best_factor = power_of_two / face.units_per_em
    return best_factor


def cround(c: complex, ndigits: Optional[int] = None) -> complex:
    return complex(round(c.real, ndigits), round(c.imag, ndigits))


def bounds(points: Iterable[complex]):
    x0 = y0 = float("+inf")
    x1 = y1 = float("-inf")

    for point in points:
        x0 = min(x0, point.real)
        y0 = min(y0, point.imag)
        x1 = max(x1, point.real)
        y1 = max(y1, point.imag)

    return complex(x0, y0), complex(x1, y1)


def guess_family_and_style(name: str, default_style: str = "Regular"):
    family = ""
    style, weight = None, None

    it = iter(reversed(re.split(r"\s+", name)))
    for word in it:
        if not weight and word.lower() in WEIGHTS:
            weight = word
        elif not style and word.lower() in STYLES:
            style = word
        else:
            family = " ".join((*reversed(list(it)), word))

    style_and_weight = " ".join(filter(None, (style, weight)))

    return family, style_and_weight.title() or default_style
