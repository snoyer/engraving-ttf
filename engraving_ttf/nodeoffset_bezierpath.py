import logging
from typing import Iterable, Iterator

from .bezierpath import BezierPathCommand, split_bezier_path
from .nodeoffset import apply_offset, revert_offset

logger = logging.getLogger(__name__)


def apply_offset_bezierpath(
    path: Iterable[BezierPathCommand], d: float
) -> Iterator[BezierPathCommand]:
    for subpath in split_bezier_path(path):
        source_onoff_path = bezier_path_to_onoff_path(subpath)
        offset_onoff_path = apply_offset(source_onoff_path, d)

        if offset_onoff_path:
            yield from bezier_path_from_onoff_path(offset_onoff_path)
            yield "Z",
        else:
            logger.warning("empty offset for: %s", subpath)


def revert_offset_bezierpath(
    path: Iterable[BezierPathCommand],
) -> Iterator[BezierPathCommand]:
    for subpath in split_bezier_path(path):
        offset_onoff_path = bezier_path_to_onoff_path(subpath)
        reverted_onoff_path = revert_offset(offset_onoff_path)
        yield from bezier_path_from_onoff_path(reverted_onoff_path)


def bezier_path_to_onoff_path(
    bezier_path: Iterable[BezierPathCommand],
) -> Iterator[tuple[complex, bool]]:
    first = 0j
    for cmd, *points in bezier_path:
        if cmd == "M":
            first = points[0]
            yield points[0], True
        elif cmd == "L":
            yield points[0], True
        elif cmd == "Q":
            yield points[0], False
            yield points[1], True
        elif cmd == "C":
            yield points[0], False
            yield points[1], False
            yield points[2], True
        elif cmd == "Z":
            yield first, True


def bezier_path_from_onoff_path(
    onoff_path: Iterable[tuple[complex, bool]],
) -> Iterator[BezierPathCommand]:
    def group():
        pts: list[complex] = []
        for pt, flag in onoff_path:
            pts.append(pt)
            if flag is True:
                yield tuple(pts)
                pts.clear()

    for i, pts in enumerate(group()):
        n = len(pts)
        if n == 1:
            yield ("L", pts[0]) if i else ("M", pts[0])
        elif n == 2:
            yield "Q", pts[0], pts[1]
        elif n == 3:
            yield "C", pts[0], pts[1], pts[2]
        else:
            raise ValueError(f"too many ({n-1}) consecutive off-curve points")
