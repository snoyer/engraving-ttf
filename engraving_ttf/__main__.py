import argparse
from functools import partial
import re
import tempfile
import xml.etree.ElementTree as ET
from pathlib import Path
from typing import Callable, Optional


from .nodeoffset_bezierpath import apply_offset_bezierpath
from .bezierpath import BezierPath, cubic_path_to_quadratic
from .svg2ttf import (
    BezierSvgFont,
    cround,
    guess_family_and_style,
    scale_for_ttf,
    svgfont_to_ttf,
)


def main(
    svgs: list[Path],
    into: Path,
    preprocess: Optional[Callable[[BezierSvgFont], BezierSvgFont]] = None,
    postprocess: Optional[Callable[[BezierSvgFont], BezierSvgFont]] = None,
):
    preprocess = preprocess or (lambda x: x)
    postprocess = postprocess or (lambda x: x)

    for i, fn in enumerate(svgs):
        svg = ET.parse(fn)
        for svgfont in BezierSvgFont.From_svg(svg):
            family, style = guess_family_and_style(svgfont.fontface.font_family)
            filename = re.sub(r"\s+", r"", f"{family}-{style}.ttf")
            ttf_path = into / filename

            print(f"[{i+1}/{len(args.svg_fonts)}] {fn} -> {ttf_path}")

            svgfont = postprocess(scale_for_ttf(preprocess(svgfont)))

            svgfont_to_ttf(svgfont, family, style, ttf_path)


def process_engraving_glyphpath(
    path: BezierPath,
    *,
    offset_distance: float = 32,
    curve_conversion_tolerance: float = 16,
):
    quad_path = BezierPath(
        cubic_path_to_quadratic(path, tolerance=curve_conversion_tolerance)
    )
    offset_quad_path = BezierPath(apply_offset_bezierpath(quad_path, offset_distance))

    return offset_quad_path.map_nodes(cround)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "svg_fonts",
        type=Path,
        nargs="+",
        metavar="SVG",
        help="SVG font files to convert",
    )
    parser.add_argument(
        "--offset",
        type=float,
        metavar="PERCENTAGE",
        help="offset distance in percentage of font height (default: %(default)g)",
        default=0.3,
    )
    parser.add_argument(
        "--into",
        type=Path,
        metavar="DIR",
        help="destination directory (default: %(default)s)",
        default=tempfile.gettempdir(),
    )

    args = parser.parse_args()
    
    def postprocess(svgfont: BezierSvgFont):
        h = abs(svgfont.fontface.ascent) + abs(svgfont.fontface.descent)
        offset_distance = h * args.offset / 100
        print(offset_distance)
        return svgfont.transformed(
            glyph_paths=partial(
                process_engraving_glyphpath,
                offset_distance=offset_distance,
                curve_conversion_tolerance=offset_distance / 2,
            )
        )

    main(args.svg_fonts, args.into, postprocess=postprocess)
