from itertools import chain
from typing import Iterable, Iterator, Literal

from .nodeoffset import revert_offset

PATH_MOVE_TO = 0
PATH_LINE_TO = 1
PATH_CURVE_TO = 2
PATH_CLOSE_PATH = 3

CairoPathCommand = tuple[Literal[0, 1, 2, 3], tuple[float, ...]]


def revert_offset_cairo(
    cairo_path: Iterable[CairoPathCommand],
) -> Iterator[CairoPathCommand]:
    for subpath in cairo_subpaths(cairo_path):
        offset_onoff_path = cairo_path_to_onoff_path(subpath)
        reverted_onoff_path = revert_offset(offset_onoff_path)
        yield from cairo_path_from_onoff_path(reverted_onoff_path)


def cairo_path_to_onoff_path(
    cairo_path: Iterable[CairoPathCommand],
) -> Iterator[tuple[complex, bool]]:
    first = 0j
    for cmd, coords in cairo_path:
        if cmd == PATH_MOVE_TO:
            first = complex(*coords[:2])
            yield complex(*coords[:2]), True
        elif cmd == PATH_LINE_TO:
            yield complex(*coords[:2]), True
        elif cmd == PATH_CURVE_TO:
            yield complex(*coords[0:2]), False
            yield complex(*coords[2:4]), False
            yield complex(*coords[4:6]), True
        elif cmd == PATH_CLOSE_PATH:
            yield first, True


def cairo_path_from_onoff_path(
    onoff_path: Iterable[tuple[complex, bool]],
) -> Iterator[CairoPathCommand]:
    def group():
        pts: list[complex] = []
        for pt, flag in onoff_path:
            pts.append(pt)
            if flag is True:
                yield tuple(pts)
                pts.clear()

    def coords(*cs: complex):
        return tuple(chain.from_iterable((c.real, c.imag) for c in cs))

    prev_point = 0j
    for i, points in enumerate(group()):
        n = len(points)
        if n == 1:
            yield PATH_LINE_TO if i else PATH_MOVE_TO, coords(points[0])
        elif n == 2:
            p0, p1, p2 = prev_point, *points
            yield PATH_CURVE_TO, coords(
                p0 + 2 / 3 * (p1 - p0),
                p2 - 2 / 3 * (p2 - p1),
                p2,
            )
        elif n == 3:
            yield PATH_CURVE_TO, coords(points[0], points[1], points[2])
        else:
            raise ValueError(f"too many ({n-1}) consecutive off-curve points")

        prev_point = points[-1]


def cairo_subpaths(
    path: Iterable[CairoPathCommand],
) -> Iterator[tuple[CairoPathCommand, ...]]:
    subpath: list[CairoPathCommand] = []
    for cmd in path:
        if subpath and cmd[0] == PATH_MOVE_TO:
            yield tuple(subpath)
            subpath.clear()
        subpath.append(cmd)
    if subpath:
        yield tuple(subpath)
