import xml.etree.ElementTree as ET
from dataclasses import dataclass, replace
from typing import Callable, Generic, Iterator, Optional, TypeVar

from typing_extensions import Self

PathType = TypeVar("PathType")


NSMAP = {
    "svg": "http://www.w3.org/2000/svg",
}


@dataclass
class SvgGlyph(Generic[PathType]):
    unicode: str
    name: str
    horiz_adv_x: float
    path: PathType

    @classmethod
    def From_etree(cls, e: ET.Element, parse_path: Callable[[str], PathType]):
        return cls(
            unicode=e.attrib["unicode"],
            name=e.get("glyph-name", ""),
            horiz_adv_x=float(e.get("horiz-adv-x", 0)),
            path=parse_path(e.get("d", "")),
        )


@dataclass
class SvgFontFace:
    font_family: str
    ascent: float
    descent: float
    cap_height: float
    x_height: float
    units_per_em: float

    @classmethod
    def From_etree(cls, e: ET.Element):
        return cls(
            font_family=e.get("font-family", ""),
            ascent=float(e.get("ascent", 0)),
            descent=float(e.get("descent", 0)),
            cap_height=float(e.get("cap-height", 0)),
            x_height=float(e.get("x-height", 0)),
            units_per_em=float(e.get("units-per-em", 1000)),
        )

    def map_heights(self, f: Callable[[float], float]):
        return replace(
            self,
            ascent=f(self.ascent),
            descent=f(self.descent),
            cap_height=f(self.cap_height),
            x_height=f(self.x_height),
            units_per_em=f(self.units_per_em),
        )


@dataclass
class SvgFont(Generic[PathType]):
    fontface: SvgFontFace
    glyphs: list[SvgGlyph[PathType]]
    horiz_adv_x: float
    metadata: str

    @classmethod
    def From_etree(cls, e: ET.Element, metadata: Optional[str] = None) -> Self:
        for found in e.findall("svg:font-face", NSMAP):
            return cls(
                fontface=SvgFontFace.From_etree(found),
                glyphs=[
                    SvgGlyph.From_etree(e, cls.Parse_path)
                    for e in e.findall("svg:glyph", NSMAP)
                ],
                horiz_adv_x=float(e.get("horiz-adv-x", 0)),
                metadata=metadata.strip() if metadata else "",
            )
        else:
            raise ValueError("no `font-face` element found")

    @classmethod
    def From_svg(cls, svg: ET.ElementTree) -> Iterator[Self]:
        found = svg.find("svg:metadata", NSMAP)
        metadata = found.text if found is not None else ""

        for font in svg.findall(".//svg:font", NSMAP):
            yield cls.From_etree(font, metadata)

    def transformed(
        self,
        heights: Callable[[float], float] = lambda x: x,
        width: Callable[[float], float] = lambda x: x,
        glyphs: Callable[[SvgGlyph[PathType]], SvgGlyph[PathType]] = lambda x: x,
        glyph_paths: Callable[[PathType], PathType] = lambda x: x,
    ) -> Self:
        new_glyphs = (glyphs(glyph) for glyph in self.glyphs)
        return replace(
            self,
            fontface=self.fontface.map_heights(heights),
            horiz_adv_x=width(self.horiz_adv_x),
            glyphs=[
                replace(glyph, path=glyph_paths(glyph.path)) for glyph in new_glyphs
            ],
        )

    @classmethod
    def Parse_path(cls, d: str) -> PathType:
        raise NotImplementedError()


class RawSvgGlyph(SvgGlyph[str]):
    """`SvgGlyph` with `.path` member as an unparsed SVG `d` string"""

    pass


class RawSvgFont(SvgFont[str]):
    """`SvgFont` with glyphs' `.path` member as an unparsed SVG `d` string"""

    @classmethod
    def Parse_path(cls, d: str) -> str:
        return d
