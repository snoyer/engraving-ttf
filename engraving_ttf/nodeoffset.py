from itertools import groupby, pairwise, repeat
from typing import Iterable, Iterator


def apply_offset(
    onoff_path: Iterable[tuple[complex, bool]],
    distance: float,
) -> tuple[tuple[complex, bool], ...]:
    points, flags = zip(*onoff_path)
    half_d = distance / 2
    normals = [n * half_d for n in polyline_normals(points, points[0] == points[-1])]
    upper = [point + normal for point, normal in zip(points, normals)]
    lower = [point - normal for point, normal in zip(points, normals)]

    return tuple((*zip(upper, flags), *reversed(list(zip(lower, flags)))))


def revert_offset(
    onoff_path: Iterable[tuple[complex, bool]]
) -> tuple[tuple[complex, bool], ...]:
    onoff_path = list(onoff_path)

    n = len(onoff_path)
    while n > 0 and onoff_path[n - 1][0] == onoff_path[0][0]:
        n -= 1

    upper = onoff_path[: (n + 1) // 2]
    lower = onoff_path[n - 1 : n // 2 - 1 : -1]  # [n//2:n][::-1]

    if any(f1 != f2 for (_, f1), (_, f2) in zip(upper, lower)):
        raise NotAnOffset("control points not symetrical")

    return tuple(((p1 + p2) / 2, f) for (p1, f), (p2, _) in zip(upper, lower))


class NotAnOffset(ValueError):
    pass


def polyline_normals(
    points: Iterable[complex], is_loop: bool = False, default: complex = 1 + 0j
) -> Iterator[complex]:
    """Compute normal direction vector for each point of a polyline."""
    tangents = polyline_tangents(points, is_loop)
    for v1, v2 in pairwise(tangents):
        v = normed(v1 + v2) or v1 or default
        yield v * 1j  # rotate 90deg


def polyline_tangents(
    points: Iterable[complex], is_loop: bool = False, default: complex = 0j
) -> Iterator[complex]:
    """Compute tangent direction vectors along a polyline.

    Given `n` points, computes the `n+1` direction vectors:
    - `v[0]` in to `p[0]`,
    - `v[1]` out from `p[0]` in to `p[1]`
    - ...
    - `v[n-1]` out from `p[n-2]` in to `p[n-1]`
    - `v[n]` out from `p[n-1]`
    """
    points, counts = run_length_encoding(points)
    n = len(points)

    if n == 1:
        yield default
        yield default
    elif n > 1:
        if is_loop:
            if points[0] == points[-1]:
                in_to_first = points[-1] - points[-2]
                out_from_last = points[1] - points[0]
            else:
                in_to_first = points[0] - points[-1]
                out_from_last = points[0] - points[-1]
        else:
            in_to_first = points[1] - points[0]
            out_from_last = points[-1] - points[-2]

        yield from repeat(normed(in_to_first), counts[0])
        for (a, b), count in zip(pairwise(points), counts[1:]):
            yield from repeat(normed(b - a), count)
        yield normed(out_from_last)


def run_length_encoding(points: Iterable[complex]) -> tuple[list[complex], list[int]]:
    keys: list[complex] = []
    counts: list[int] = []
    for k, vs in groupby(points):
        keys.append(k)
        counts.append(len(list(vs)))
    return keys, counts


def normed(v: complex) -> complex:
    return (v / abs(v)) if v else v
