from itertools import chain
from typing import Iterable, Iterator, Union
from inkex.paths import Path, Move, Line, Curve, Quadratic, ZoneClose

from .nodeoffset import revert_offset


def revert_offset_inkex(inkex_path: Path) -> Path:
    def commands():
        for subpath in inkex_path.break_apart():
            offset_onoff_path = inkex_path_to_onoff_path(subpath)
            reverted_onoff_path = revert_offset(offset_onoff_path)
            yield from inkex_path_from_onoff_path(reverted_onoff_path)

    return Path(commands())


def inkex_path_to_onoff_path(inkex_path: Path) -> Iterator[tuple[complex, bool]]:
    first = 0j
    for command in inkex_path.to_absolute().to_non_shorthand():
        if isinstance(command, Move):
            first = complex(command.x, command.y)
            yield complex(command.x, command.y), True
        elif isinstance(command, Line):
            yield complex(command.x, command.y), True
        elif isinstance(command, Quadratic):
            yield complex(command.x2, command.y2), False
            yield complex(command.x3, command.y3), True
        elif isinstance(command, Curve):
            yield complex(command.x2, command.y2), False
            yield complex(command.x3, command.y3), False
            yield complex(command.x4, command.y4), True
        elif isinstance(command, ZoneClose):
            yield first, True
            pass
        else:
            raise ValueError(f"unexpected {type(command).__name__} command")


def inkex_path_from_onoff_path(
    onoff_path: Iterable[tuple[complex, bool]],
) -> Iterator[Union[Line, Move, Quadratic, Curve]]:
    def group():
        pts: list[complex] = []
        for pt, flag in onoff_path:
            pts.append(pt)
            if flag is True:
                yield tuple(pts)
                pts.clear()

    def coords(*cs: complex):
        return tuple(chain.from_iterable((c.real, c.imag) for c in cs))

    for i, points in enumerate(group()):
        n = len(points)
        if n == 1:
            yield Line(*coords(points[0])) if i else Move(*coords(points[0]))
        elif n == 2:
            yield Quadratic(*coords(points[0]))
        elif n == 3:
            yield Curve(*coords(points[0], points[1], points[2]))
        else:
            raise ValueError(f"too many ({n-1}) consecutive off-curve points")
