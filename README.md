# Engraving TTF


The goal of this project is to allow the use of engraving fonts with traditional text pipelines in existing software.

Engraving fonts use open strokes in order to be plotted/engraved,
however conventional fonts use closed contours in order to be rasterized.

We propose a method and tool to convert engraving open-strokes SVG fonts to traditional closed-contours TrueType fonts such that:
1. rendered glyphs can be post-processed back into their original strokes in order to be optimally engraved,
2. glyphs rasterize acceptably as filled shapes in order to be displayed and edited using existing software,
3. glyphs can be engraved as stroked shapes, as a fallback in case a post-processing tool is not available.



## Method

If we were to turn an open stroke into a closed shape for ideal rendering we would want a true parallel offset,
however we are aiming at being able to post-process the final glyphs back into their original strokes.

A trivial solution would be to double-up, or rather double-back, each stroke by concatenating it with a reversed copy of itself.
This is easily reversible as discarding the second half of each sub-path to yield the original stroke back.
This however produces shapes of zero width that would not be visible when rasterized as filled shapes.

In order to give the glyphs some width but still allow for simple post-processing
we create a pair of offset strokes by shifting the bezier nodes a (small) constant distance in opposite directions.
The original curve can be retrieved by taking the mid-point of fist and last, second and second-to-last, ... pairs of nodes.
While not being a true parallel offset this method still rasterizes acceptably, especially given that the offset distance is kept small in practice.

![method](examples/method.svg)



## Fonts Generation

The `engraving_ttf` Python module depends on `fontTools` to generate TTF fonts from SVG fonts.

The following command converts the SVG fonts from [Evil Mad Scientist Laboratories' `SVG Fonts` project](https://gitlab.com/oskay/svg-fonts) to TTF files in the user's local fonts directory.

    python -m engraving_ttf ../svg-fonts/fonts/**.svg --into=~/.local/share/fonts
    fc-cache -f ~/.local/share/fonts   # force update font cache



## Post-processing

As a proof of concept, we provide an Inkscape extension that can reverse the offsetting process and produce glyphs ready to be plotted/engraved.

The extension first convert the selected text elements to paths (using the `object-to-path` action, having the same effect as from the `Path > Object to Path` menu) and then applies the un-offsetting process to each resulting path element.
Ideally this post-processing would be better implemented using Live Effects rather than an extension but they are not supported on text objects at the time of this writing :(

The following screenshot shows [an example](examples/inkscape-example.svg) of text being edited in Inkscape using various typography and layout features.
These features are retained by [the engravable text after post-processing](examples/inkscape-example-post.svg).

![text being edited using Inkscape text tools](examples/inkscape-example-screenshot.png)  
