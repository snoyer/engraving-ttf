import subprocess
import tempfile
from itertools import chain
from typing import Iterable, Iterator, Union

import inkex
from inkex.paths import Curve, Line, Move, Path, Quadratic, ZoneClose


class EngravingTtfEffect(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)

        self.arg_parser.add_argument(
            "--default-all",
            type=inkex.Boolean,
            default=True,
            help="Process all texts if selection empty",
        )
        self.arg_parser.add_argument(
            "--delete-text",
            type=inkex.Boolean,
            default=False,
            help="Delete original text",
        )
        self.arg_parser.add_argument(
            "--unique", type=inkex.Boolean, default=True, help="Delete previous results"
        )
        self.arg_parser.add_argument(
            "--success-color",
            type=str,
            default="blue",
            help="Color for successfully converted paths",
        )
        self.arg_parser.add_argument(
            "--error-color",
            type=str,
            default="red",
            help="Color for unsuccessfully converted paths",
        )

    def effect(self):
        texts = [
            t
            for t in self.selected_texts(default_all=self.options.default_all)
            if t.get("id")
        ]
        paths = objects_to_path(self.document, texts)

        for text, converted_text in zip(texts, paths):
            paths = (
                [converted_text]
                if isinstance(converted_text, inkex.PathElement)
                else converted_text.xpath(".//svg:path", namespaces=inkex.NSS)
            )
            for node in paths:
                inkex_path = inkex.paths.Path(node.get("d"))
                if inkex_path is not None:
                    try:
                        new_inkex_path = revert_offset_inkex(inkex_path)
                        # print(new_inkex_path, file=stderr)
                        node.set("d", str(new_inkex_path))

                        style = inkex.styles.Style(node.get("style"))
                        style["stroke"] = self.options.success_color
                        node.set("style", str(style))
                    except NotAnOffset as e:
                        style = inkex.styles.Style(node.get("style"))
                        style["stroke"] = self.options.error_color
                        node.set("style", str(style))

            style = inkex.styles.Style(converted_text.get("style"))
            style["fill"] = "none"
            converted_text.set("style", str(style))

            new_id = text.get("id") + "-engraving"
            if self.options.unique:
                for e in self.svg.xpath('//*[@id="%s"]' % new_id, namespaces=inkex.NSS):
                    e.getparent().remove(e)
            converted_text.set("id", new_id)

            text.addnext(converted_text)

            if self.options.delete_text:
                text.getparent().remove(text)

    def selected_texts(self, default_all=False):
        text_tags = "text", "flowRoot"

        text_tags_ns = [inkex.addNS(tag, "svg") for tag in text_tags]
        text_xpath = "|".join(".//svg:%s" % tag for tag in text_tags)

        if self.svg.selected:
            for node_id, node in self.svg.selected.items():
                if node.tag in text_tags_ns:
                    yield node
                else:
                    for path_node in node.xpath(text_xpath, namespaces=inkex.NSS):
                        yield path_node
        elif default_all:
            for path_node in self.document.xpath(text_xpath, namespaces=inkex.NSS):
                yield path_node


def objects_to_path(document, nodes):
    if nodes:
        node_ids = [node.get("id") for node in nodes]

        inkscape_bin = "inkscape"  # TODO un-hardcode?

        def inkscape_actions():
            for node_id in filter(None, node_ids):
                yield "select:%s" % node_id
            yield "object-to-path"
            yield "export-filename:" + tmp.name
            yield "export-do"

        with tempfile.NamedTemporaryFile(suffix=".svg") as tmp:
            document.write(tmp)
            tmp.flush()

            inkscape_cmd = [
                inkscape_bin,
                "--actions=" + ";".join(inkscape_actions()),
                tmp.name,
            ]
            proc = subprocess.Popen(
                inkscape_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
            )
            try:
                outs, errs = proc.communicate(timeout=180)
            except TimeoutExpired:
                proc.kill()
                outs, errs = proc.communicate()
            # TODO check retcode and errors

            svg = inkex.elements.load_svg(tmp.name)

            nodes_xpath = "|".join('//*[@id="%s"]' % node_id for node_id in node_ids)
            return svg.getroot().xpath(nodes_xpath, namespaces=inkex.NSS)
    else:
        return []


########################################################################################


def revert_offset_inkex(inkex_path: Path) -> Path:
    def commands():
        for subpath in inkex_path.break_apart():
            offset_onoff_path = inkex_path_to_onoff_path(subpath)
            reverted_onoff_path = revert_offset(offset_onoff_path)
            yield from inkex_path_from_onoff_path(reverted_onoff_path)

    return Path(commands())


def inkex_path_to_onoff_path(inkex_path: Path) -> Iterator[tuple[complex, bool]]:
    first = 0j
    for command in inkex_path.to_absolute().to_non_shorthand():
        if isinstance(command, Move):
            first = complex(command.x, command.y)
            yield complex(command.x, command.y), True
        elif isinstance(command, Line):
            yield complex(command.x, command.y), True
        elif isinstance(command, Quadratic):
            yield complex(command.x2, command.y2), False
            yield complex(command.x3, command.y3), True
        elif isinstance(command, Curve):
            yield complex(command.x2, command.y2), False
            yield complex(command.x3, command.y3), False
            yield complex(command.x4, command.y4), True
        elif isinstance(command, ZoneClose):
            yield first, True
            pass
        else:
            raise ValueError(f"unexpected {type(command).__name__} command")


def inkex_path_from_onoff_path(
    onoff_path: Iterable[tuple[complex, bool]],
) -> Iterator[Union[Line, Move, Quadratic, Curve]]:
    def group():
        pts: list[complex] = []
        for pt, flag in onoff_path:
            pts.append(pt)
            if flag is True:
                yield tuple(pts)
                pts.clear()

    def coords(*cs: complex):
        return tuple(chain.from_iterable((c.real, c.imag) for c in cs))

    for i, points in enumerate(group()):
        n = len(points)
        if n == 1:
            yield Line(*coords(points[0])) if i else Move(*coords(points[0]))
        elif n == 2:
            yield Quadratic(*coords(points[0]))
        elif n == 3:
            yield Curve(*coords(points[0], points[1], points[2]))
        else:
            raise ValueError(f"too many ({n-1}) consecutive off-curve points")


########################################################################################


def revert_offset(
    onoff_path: Iterable[tuple[complex, bool]]
) -> tuple[tuple[complex, bool], ...]:
    onoff_path = list(onoff_path)

    n = len(onoff_path)
    while n > 0 and onoff_path[n - 1][0] == onoff_path[0][0]:
        n -= 1

    upper = onoff_path[: (n + 1) // 2]
    lower = onoff_path[n - 1 : n // 2 - 1 : -1]  # [n//2:n][::-1]

    if any(f1 != f2 for (_, f1), (_, f2) in zip(upper, lower)):
        raise NotAnOffset("control points not symetrical")

    return tuple(((p1 + p2) / 2, f) for (p1, f), (p2, _) in zip(upper, lower))


class NotAnOffset(ValueError):
    pass


########################################################################################


if __name__ == "__main__":
    e = EngravingTtfEffect()
    e.run()
