from setuptools import setup

setup(
    name='engraving-ttf',
    version='0.1.0',
    packages=['engraving_ttf'],
    install_requires=[
        'typing_extensions',
        'fontTools',
    ],
)